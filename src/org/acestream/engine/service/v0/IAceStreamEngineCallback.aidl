/**
 * Copyright (C) 2013, ACEStream. All rights reserved.
 */

package org.acestream.engine.service.v0;

/**
 * AceStream callback interface
 */
oneway interface IAceStreamEngineCallback {

	/**
	 * Service start unpack additional files if needed.
	 * Unpacking may be need after installation or updating.
	 */
	void onUnpacking();
	
	/**
	 * Service starting Ace Stream engine and try connect to it.
	 */
	void onStarting();
	
	/**
	 * Ace Stream engine started successfully and listens @listenPort 
	 * or failed to start,  if @listenPort == -1.
	 */
	void onReady(int listenPort);
	
	/**
	 * Ace Stream engine stopped.
	 */
	void onStopped();
	
	/**
	 * Internet connection is not available.
	 * Engine will start when internet connection is available.
	 */
	void onWaitForNetworkConnection();
}