package org.acestream.engine.service.example.client;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class AceStreamClientExample extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

}
