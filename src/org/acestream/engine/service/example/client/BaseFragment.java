package org.acestream.engine.service.example.client;

import java.util.ArrayList;

import org.acestream.engine.service.v0.AceStreamEngineMessages;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public abstract class BaseFragment extends Fragment {
	
	private final ServiceMessagesHandler mMessagesHandler = new ServiceMessagesHandler(this) ; 
	
	private static class ServiceMessagesHandler extends Handler {

		private BaseFragment mParent;
		public ServiceMessagesHandler(BaseFragment parent) {
			mParent = parent;
		}

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case AceStreamEngineMessages.MSG_ENGINE_UNPACKING:
				mParent.addListValue("UNPACKING");
				break;
			case AceStreamEngineMessages.MSG_ENGINE_STARTING:
				mParent.addListValue("STARTING");
				break;
			case AceStreamEngineMessages.MSG_ENGINE_READY:
				mParent.addListValue("READY port=" + String.valueOf(msg.arg1));
				break;
			case AceStreamEngineMessages.MSG_ENGINE_STOPPED:
				mParent.addListValue("STOPPED");
				break;
			case AceStreamEngineMessages.MSG_ENGINE_WAIT_CONNECTION:
				mParent.addListValue("CONNECTION");
				break;
			default:
				break;
			}
		}
	};
	
	private ArrayList<String> mListItems = new ArrayList<String>();
	private ArrayAdapter<String> mListAdapter;
	private ListView mListView;
	
	private BaseClient mServiceClient;

	public void initServiceClient(BaseClient client) {
		mServiceClient = client;
		mServiceClient.setHandler(mMessagesHandler);
	}
	
	public void initView(View view) {
		mListView = (ListView)view.findViewById(R.id.service_msg_list);
		mListAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_list_item_1, mListItems);
		mListView.setAdapter(mListAdapter);
		
		Button btnBind = (Button)view.findViewById(R.id.btn_bind);
		btnBind.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mServiceClient.bind();
			}
		});
		
		Button btnUnbind = (Button)view.findViewById(R.id.btn_unbind);
		btnUnbind.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mServiceClient.unbind();
			}
		});
	}

	public void addListValue(String text) {
		mListItems.add(text);
		mListAdapter.notifyDataSetChanged();
	}
	
	@Override
	public void onDestroy() {
		mServiceClient.unbind();
		super.onDestroy();
	}
}
