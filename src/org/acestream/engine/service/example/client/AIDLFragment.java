package org.acestream.engine.service.example.client;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AIDLFragment extends BaseFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_aidl, container, false);

		initServiceClient(new AIDLClient(getActivity().getApplicationContext()));
		initView(view);

		setRetainInstance(true);
		return view;
	}
	
}
